import axios from "axios";

// 添加电影基本信息
export function insertMovInfo(movie) {
    return new Promise((resolve, reject) => {
        axios
            .post("/admin/insertMovInfo", { ...movie })
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err);
            });
    });
}
// 更新电影基本信息
export function updateMovInfo(movie) {
    return new Promise((resolve, reject) => {
        axios
            .post("/admin/updateMovInfo", { ...movie })
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err);
            });
    });
}
// 删除电影基本信息
export function delMovInfo(movieId){
    return new Promise((resolve, reject) => {
        axios
            .get(`/admin/delMovInfo/${movieId}`)
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err);
            });
    });
}
// 获取全部订单信息
export function getAllOrderInfo(pageData) {
    return new Promise((resolve, reject) => {
        axios
            .post("/admin/orderInfo", { ...pageData })
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err);
            });
    });
}
// 获取全部用户信息
export function getAllUserInfo(pageData) {
    return new Promise((resolve, reject) => {
        axios
            .post("/admin/userInfo", { ...pageData })
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err);
            });
    });
}

