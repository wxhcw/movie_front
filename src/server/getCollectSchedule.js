import axios from "axios";

// 查看收藏的场次信息
export function getCollectSchedule() {
    return new Promise((resolve, reject) => {
        axios
            .get("/movie/collect/schedule")
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err);
            });
    });
}
